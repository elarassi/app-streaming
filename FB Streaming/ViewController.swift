//
//  ViewController.swift
//  FB Streaming
//
//  Created by ABDELAZiZ EL ARASSi on 7/2/20.
//  Copyright © 2020 ABDELAZiZ EL ARASSi. All rights reserved.
//

import UIKit
import AVFoundation
import HaishinKit

class ViewController: UIViewController {

    @IBOutlet weak var recordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    @IBAction func actionRecordButton(_ sender: UIButton) {
        print("recordButton")
        self.StreamYoutube()
//        self.StreamYoutube2()
        self.StreamFB()
    }
    
    

    @objc
    func rtmpStatusHandler(_ notification: Notification) {
        let rtmpConnectionFB = RTMPConnection();
        let rtmpStreamFB = RTMPStream(connection: rtmpConnectionFB)
        
        let rtmpConnectionY1 = RTMPConnection();
        let rtmpStreamY2 = RTMPStream(connection: rtmpConnectionY1)
        
        
        let e = Event.from(notification)
        if let data: ASObject = e.data as? ASObject, let code: String = data["code"] as? String {
            switch code {
            case RTMPConnection.Code.connectSuccess.rawValue:
                rtmpStreamFB.publish("668617827319549?s_bl=1&s_ps=1&s_sw=0&s_vt=api-s&a=Abw-Yyly4Fdqt4qe")
                rtmpStreamY2.publish("kke9-tuac-dtha-ftbd")
                // sharedObject!.connect(rtmpConnection)
            default:
                break
            }
        }
    }
    
    
    func StreamFB() {
        
        let rtmpConnectionFB = RTMPConnection()
        let rtmpStreamFB = RTMPStream(connection: rtmpConnectionFB)
//
        rtmpStreamFB.attachAudio(AVCaptureDevice.default(for: AVMediaType.audio)) { error in
            print("error 1 \(error)")
        }
        rtmpStreamFB.attachCamera(DeviceUtil.device(withPosition: .back)) { error in
            print("error 2 \(error)")
        }

        let hkView = HKView(frame: view.bounds)
        hkView.videoGravity = AVLayerVideoGravity.resizeAspectFill
        hkView.attachStream(rtmpStreamFB)

        // add ViewController#view
        view.addSubview(hkView)

        rtmpConnectionFB.connect("rtmps://live-api-s.facebook.com:443/rtmp")
        rtmpStreamFB.publish("668617827319549?s_bl=1&s_ps=1&s_sw=0&s_vt=api-s&a=Abw-Yyly4Fdqt4qe")
        
        // if you want to record a stream.
        // rtmpStream.publish("streamName", type: .localRecord)
    }
    
    func StreamYoutube() {
           
           let rtmpConnection = RTMPConnection()
           let rtmpStream = RTMPStream(connection: rtmpConnection)
           rtmpStream.attachAudio(AVCaptureDevice.default(for: AVMediaType.audio)) { error in
               print("error 1 \(error)")
           }
           rtmpStream.attachCamera(DeviceUtil.device(withPosition: .back)) { error in
               print("error 2 \(error)")
           }

           let hkView = HKView(frame: view.bounds)
           hkView.videoGravity = AVLayerVideoGravity.resizeAspectFill
           hkView.attachStream(rtmpStream)

           // add ViewController#view
           view.addSubview(hkView)

           rtmpConnection.connect("rtmp://x.rtmp.youtube.com/live2")
            rtmpConnection.addEventListener(.rtmpStatus, selector: #selector(rtmpStatusHandler), observer: self)
//           rtmpStream.publish("kke9-tuac-dtha-ftbd")
           
           // if you want to record a stream.
           // rtmpStream.publish("streamName", type: .localRecord)
       }
    
    func StreamYoutube2() {
        
        let rtmpConnection2 = RTMPConnection()
        let rtmpStream2 = RTMPStream(connection: rtmpConnection2)
        rtmpStream2.attachAudio(AVCaptureDevice.default(for: AVMediaType.audio)) { error in
            print("error 1 \(error)")
        }
        rtmpStream2.attachCamera(DeviceUtil.device(withPosition: .back)) { error in
            print("error 2 \(error)")
        }

        let hkView = HKView(frame: view.bounds)
        hkView.videoGravity = AVLayerVideoGravity.resizeAspectFill
        hkView.attachStream(rtmpStream2)

        // add ViewController#view
        view.addSubview(hkView)

        rtmpConnection2.connect("rtmp://a.rtmp.youtube.com/live2")
        rtmpStream2.publish("26w2-k4yq-t3h6-dbpz-0d8j")
        
        // if you want to record a stream.
        // rtmpStream.publish("streamName", type: .localRecord)
    }
    
}

